//Chris Hinson
//Chapter 4 Program 11
//This class handles formatting the output

public class output {
    public static void printCandF()
    {
        convert converter = new convert();

        System.out.println("Celsius     Fahrenheit");
        System.out.println("----------------------");

        for (int i=0;i<=20;i++)
        {
            System.out.println(i + "     " + converter.cToF(i));

        }
    }
}
